# example of "switch" control statement in Python
# (which does not exist)
# The "Pythonic" way of doing this is with a dictionary


switcher = {'jackie': 'jack', 'joe': 'joseph', 'jo': 'jolinda'}


def foo(name):
    if name in switcher:
        print('{} is a contraction of {}'.format(name, switcher[name]))
    else:
        print('{} is not known'.format(name))


foo('sam')
foo('joe')
foo('jo')
