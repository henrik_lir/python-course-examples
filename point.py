# example class Point
# assignment: extend this with __add__ and __mul__

from math import sqrt


class Point:
    def __init__(self, x, y, z=0):
        self.x, self.y, self.z = x, y, z

    def __str__(self):
        return ('Point({}, {}, {})'.format(self.x, self.y, self.z))

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def length(self):
        return sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    def __gt__(self, other):
        return self.length() > other.length()

    def __lt__(self, other):
        return self.length() < other.length()



p1 = Point(5, 0)
p2 = Point(0, 8)
 
print(p1)
print(p2)
print('length p1: ' + str(p1.length()))
print('p1 > p2? ', p1 > p2)
print('p1 < p2? ', p1 < p2)
print('p1 == p2? ', p1 == p2)
