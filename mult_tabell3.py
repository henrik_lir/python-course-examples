# multiplication table.  Very Pythonic.
# author: Erik Nash


for List in [[a*b for b in range(1, 10)] for a in range(1, 10)]:
    print(List)
