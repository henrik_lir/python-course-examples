# This displays a simple message window
# when function "message1" is called.
# Typically this module is imported by another module.


#print('begin import message1.py')


def foo(msg):
    print(msg)


#print('end import message1.py')


# Har finns en "lambda" funktion:
def foo2(msg): return print(msg)


def info(msg): return print('Note: ' + msg)
def warning(msg): return print('Warning: ' + msg)
