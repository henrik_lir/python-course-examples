# Tuples are immutable objects that are very useful.

tpl = ('How', 'do', 'you', 'do?')
print()
print('Here is the tuple:')
print(tpl)
print(' '.join(tpl))

# Here we print various slices
print()
print('Here we print various subsets of the list:')
print(' '.join(tpl[:3]))
print(' '.join(tpl[1:]))
print(' '.join(tpl[::2]))
print(' '.join(tpl[::3]))

# Here we print the entire list:
print()
print('Here we print the entire list one item at a time:')
for item in tpl:
    print(item)
