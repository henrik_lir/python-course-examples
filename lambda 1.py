# This shows how to use a lambda function


def fmult1(a, b):
    z = a * b
    return z


def fmult2(a, b):
    return a * b


def fmult3(a, b): return a * b


def fmult4(a, b): return a * b


print(fmult1(3, 4))
print(fmult2(3, 4))
print(fmult3(3, 4))
print(fmult4(3, 4))
