# Example from
# https://python-3-patterns-idioms-test.readthedocs.io/en/latest/PythonForProgrammers.html#classes
# showing inheritance ("derived class")

from Class2 import Simple


class Simple2(Simple):
    def __init__(self, str):
        print("Inside the Simple2 constructor")
        Simple.__init__(self, str)

    def display(self):
        self.showMsg("Method simple2 display()")

    def show(self):
        print("Overridden show() method")
        Simple.show(self)


class Different:
    def show(self):
        print("Not derived from Simple")


if __name__ == "__main__":
    x = Simple2("Simple2 argument")
    print(x)
    x.display()
    x.show()
    x.showMsg("Inside main")
    def f(obj): obj.show()  # One-line definition
    f(x)
    f(Different())
