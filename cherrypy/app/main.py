#!/usr/bin/env python3
#
# https://docs.cherrypy.org/en/latest/tutorials.html#tutorial-6-what-about-my-javascripts-css-and-images


import os.path
import sys
import cherrypy


class IndexPage:

    @cherrypy.expose
    def index(self):
        # Ask for the user's name.
        with open("res/index.html", "r") as f:
            s = f.read()
        return s                              # serving a string
        #return open("res/index.html", "r")   # serving an open file

    @cherrypy.expose
    def greetUser(self, name=None):
        # test user name.  If defined, say hoi.
        if name:
            # Greet the user!
            return "Hoi %s, what's up?" % name
        else:
            if name is None:
                # No name was specified
                return 'Please enter your name <a href="./">here</a>.'
            else:
                return 'No, really, enter your name <a href="./">here</a>.'


mainconf = os.path.join(os.path.dirname(__file__), 'main.conf')

conf = {
    '/': {
        'tools.sessions.on': True,
        'tools.staticdir.root': os.path.abspath(os.getcwd())
    },
    '/static': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './res'
    }
}


def main(arg):
    #cherrypy.quickstart(IndexPage(), config=mainconf)
    #cherrypy.quickstart(IndexPage(), "/", conf)
    cherrypy.quickstart(IndexPage(), "/", mainconf)
    return


if __name__ == "__main__":
    main(sys.argv)
