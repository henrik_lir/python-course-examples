# Sample text input skript
# type casting, switch, calling a function, string split


def mult(a, b):
    print(float(a) * float(b))


def add(a, b):
    print(float(a) + float(b))


def div(a, b): return print(float(a) / float(b))


def loop():
    switch = {'mult': mult, 'add': add, 'div': div}
    done = False
    while not done:
        c = input("cmd> ")
        c = c.split(" ")
        while len(c) < 3:
            c.append(0)
        if c[0] in switch:
            foo = switch[c[0]]
            foo(*c[1:])
        elif c[0] == 'quit':
            done = True
        else:
            print("unknown command")


loop()
