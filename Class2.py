# This shows a simple class with ways to show contents of variables


class Simple:
    def __init__(self, s):
        print("Inside the Simple constructor")
        self.s = s

    def __str__(self):
        return 'Simple("{}")'.format(str(self.s))

    def show(self):
        print('method show: ' + str(self.s))

    def showMsg(self, msg):
        print(msg + ': ' + str(self.s))


if __name__ == "__main__":
    # Create an object:
    x = Simple("constructor argument")
    print(x)
    x.show()
    x.showMsg("A message")
