# example of if..then..else


for name in ['sam', 'joe', 'jo', 'sam']:
    if name == 'sam':
        print('we found "sam"')
    elif name == 'joe':
        print('we found "joe"')
    else:
        print('another name: ' + name)
