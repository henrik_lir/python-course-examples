# This shows how to use a lambda function


def print_table(foo, title):
    print(title)
    lst = [[foo(x, y) for x in range(1, 10)] for y in range(1, 10)]
    s = '\t' + '\t'.join([str(i) for i in range(1, 10)]) + '\n'
    i = 0
    for b in lst:
        i += 1
        s += '{}\t'.format(i)
        s += '\t'.join([str(a) for a in b]) + '\n'
    print(s)


def fmult(a, b): return a * b


print_table(fmult, 'multiplikation')


def fadd(a, b): return a + b


print_table(fadd, 'addition')


def fdiv(a, b): return round(a / b, 4)


print_table(fdiv, 'division')


def fmodulo(a, b): return a % b


print_table(fmodulo, 'modulo')
